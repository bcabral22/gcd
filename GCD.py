#function to get the GCD using the euclidean algorithm 
def GCDEuclidean(a,b):
    #string
    print("Here are the intial values:\nx=",a)
    #string
    print("y=",b)
    #while loop, keeping doing this until mod is 0
    while True:
        #geting the value of a mod b and storing it in c
        c= a % b
        #if c = 0 then break while loop
        if c== 0:
            break 
        #else c is not 0 then make the a value equal to b  
        # and make the b value equal to c. essentially swapping until c = 0
        else: 
            a=b
            b=c
    #print the GCD which would be the last b when c hit 0
    print("\nGCD=",b)

#call and run the function 
GCDEuclidean(55539,22627)    

